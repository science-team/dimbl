Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://github.com/LanguageMachines/dimbl

Files: *
Copyright: 1998 - 2023 the ILK Research Group (Tilburg University, The Netherlands)
                       Lead programmer: Ko van der Sloot; Code, algorithm, and design
                       contributions by: Antal van den Bosch, Walter Daelemans, Jakub Zavrel
License: GPL-3+

Files: debian/*
Copyright: 2010-3013 Ko van der Sloot <Ko.vanderSloot@uvt.nl>
License: GPL-3+

Files: m4/pkg.m4
Author: Scott James Remnant
Copyright: Copyright © 2004 Scott James Remnant <scott@netsplit.com>
License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 The complete text of the GNU General Public License, Version 2, can
 be found in `/usr/share/common-licenses/GPL-2'.

License: GPL-3+
 Dimbl is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 Dimbl is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General Public
 License, Version 3 can be found in `/usr/share/common-licenses/GPL-3'.
